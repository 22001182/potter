//
//  ViewController.swift
//  Atividade
//
//  Created by COTEMIG on 27/10/22.
//


import UIKit
import Alamofire
import Kingfisher
import SwiftUI


struct atriz: Decodable {
    let name : String
    let actor: String
    let image: String
    
}
class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDePersonagens: [atriz]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDePersonagens?.count ?? 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "API", for: indexPath) as! Mycell
               let actor = listaDePersonagens![indexPath.row]
               
               cell.Label1.text = actor.name
               cell.Label2.text = actor.actor
               cell.imagem.kf.setImage(with: URL(string: actor.image))
               
               return cell

    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data()
        tableView.dataSource = self

    }
    
    private func data(){
        
            AF.request("https://hp-api.herokuapp.com/api/characters")
                .responseDecodable(of: [atriz].self){
                response in
                    if let lista_Personagens = response.value{
                        self.listaDePersonagens = lista_Personagens
                }
                    self.tableView.reloadData()
            }
    }
}

